package com.t1.yd.tm.model;

import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;

public class Command {

    public static final Command HELP = new Command(ArgumentConstant.HELP, CommandConstant.HELP, "Show info about program");
    public static final Command ABOUT = new Command(ArgumentConstant.ABOUT, CommandConstant.ABOUT, "Show info about program");
    public static final Command VERSION = new Command(ArgumentConstant.VERSION, CommandConstant.VERSION, "Show program version");
    public static final Command INFO = new Command(ArgumentConstant.INFO, CommandConstant.INFO, "Show system info");
    public static final Command EXIT = new Command(null, CommandConstant.EXIT, "Exit program");

    private String argument;
    private String name;
    private String description;

    public Command() {
    }

    public Command(String argument, String name, String description) {
        this.argument = argument;
        this.name = name;
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("%s%s : %s", name, argument != null ? ", " + argument : "", description);
    }
}
